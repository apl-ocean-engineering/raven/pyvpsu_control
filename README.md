# pyvpsu_control

Implements the Raven ROV-specific elements for controlling the VPSUs
through the Netburner serial-to-ethernet converter on the Vehicle Interface Board.
Actual hardware interface is handled by [pynetburner_gpio](https://gitlab.com/apl-ocean-engineering/pynetburner_gpio).

This package is pure Python, and is used by the ROS package [vpsu_enable_driver](https://gitlab.com/apl-ocean-engineering/raven/vpsu_enable_driver).

For more details, see the [VPSU Enable page](https://gitlab.com/apl-ocean-engineering/raven/raven_wiki/-/wikis/Hardware/VPSU-Enable) in the Raven Wiki).

# License

Released under the [BSD 3-clause license](LICENSE.txt)
