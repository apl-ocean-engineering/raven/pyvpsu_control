#!/usr/bin/env python3

import argparse
from pyvpsu_control.vpsu_control import VPSUControl
import serial

def cli_status(args, vpsu):
    status = vpsu.status()
    for key,val in status.items():
        print("%-2d : %15s %10s" % (key, val['name'], val['enabled']))

def cli_defaults(args, vpsu):
    for pin in VPSUControl.All:
        vpsu.netburner.pin(pin).set_value(VPSUControl.Disabled)
        vpsu.netburner.pin(pin).set_direction(input=False)
        vpsu.netburner.pin(pin).set_mode(alt_func=False)
    vpsu.netburner.apply()
    vpsu.netburner.save_as_defaults()


def cli_enable(args, vpsu):

    gpios = VPSUControl.parse_circuits(args.circuits)
    print("Enabling circuits: %s" % gpios)
    vpsu.enable(gpios)

def cli_disable(args, vpsu):
    gpios = VPSUControl.parse_circuits(args.circuits)
    print("Disabling circuits: %s" % gpios)
    vpsu.disable(gpios)


def main():

    parser = argparse.ArgumentParser(description='Simple command line interfacing with VPSU enable.')

    # Global argparse arguments
    parser.add_argument("--host",
                        default="10.10.10.134",
                        help="Hostname/IP address for Netburner (default: 10.10.10.134)")
    parser.add_argument("--port",
                        default=1000,
                        help="Port for interface (default: 1000")

    subparser = parser.add_subparsers(required=True,dest='command')

    subparser.add_parser('status',help="Returns the current status of the VPSU enables").set_defaults(func=cli_status)

    subparser.add_parser("program-defaults", help="Resets the VIB to the standard power-on defaults: all circuits off").set_defaults(func=cli_defaults)

    output_parser = subparser.add_parser('enable', help="Enable one or more VPSU circuits")
    output_parser.set_defaults( func=cli_enable )
    output_parser.add_argument("circuits", nargs="*", default=["thrusters"], help="Circuits to enable (defaults to \"thrusters\").  Valid values: a8, a9, thrusters, arms, all")

    output_parser = subparser.add_parser('disable', help="Disable one or more VPSU circuits")
    output_parser.set_defaults( func=cli_disable )
    output_parser.add_argument("circuits", nargs="*", default=["all"], help="Circuits to disable (default: \"all\").  Valid values: a8, a9, thrusters, arms, all")

    args = parser.parse_args()

    try:
        vpsu = VPSUControl( host=args.host, port=args.port )
    except serial.serialutil.SerialException:
        print("Unable to connect to VPSU,  is it powered and on the network?")
        exit(-1)

    args.func(args, vpsu)

if __name__=="main":
    main()