from collections.abc import Iterable

from pynetburner_gpio.netburner import Netburner

## Mappings to GPIO pins
A8_Thrusters = 14
A9_Thrusters = 9
A8_Arms = 10

All_Thrusters = [ A8_Thrusters, A9_Thrusters ]
All = All_Thrusters + [A8_Arms]

Valid_Entries = All

Enable_Names = { A8_Thrusters: "A8 Thrusters",
                 A9_Thrusters: "A9 Thrusters",
                 A8_Arms: "A8 Arms" }

Enabled = 0
Disabled = 1


def get_iterable(x):
    if isinstance(x, Iterable):
        return x
    else:
        return (x,)


class VPSUControl:

    def __init__(self, host="10.10.10.134", port=1000, fp=None):

        self.netburner = Netburner(host,port,fp)

    def parse_circuits(circuits):

        out = []

        for c in circuits:
            c = c.lower()

            if c == "a8":
                return [A8_Thrusters]
            elif c == "a9":
                return [A9_Thrusters]
            elif c == "thrusters" or c == "thruster":
                return All_Thrusters
            elif c == "arms":
                return [A8_Arms]
            elif c == "all":
                return All
            else:
                raise ValueError("Unknown circuit \"%s\"" % c)


    def validate(self, which):
        if which is None or which not in Valid_Entries:
            assert ValueError("Unexpected GPIO %d" % gpio)

    def enable( self, which ):
        
        for gpio in get_iterable( which ):
            self.validate(gpio)
            self.netburner.pin(gpio).set_value( Enabled )

        self.netburner.apply()

    def disable( self, which ):

        for gpio in get_iterable( which ):
            self.validate(gpio)
            self.netburner.pin(gpio).set_value( Disabled )

        self.netburner.apply()


    def status( self ):

        status = {}
        for gpio in All:
            ## Value==1 means disabled.   Value==0 means enabled
            enabled = False if self.netburner.pin(gpio).value else True
            status[gpio] = { "name": Enable_Names[gpio],
                                                "enabled": enabled }

        return status
        

