import serial
import pyvpsu_control

def mock_read_until(self):
    return b'0,FFFF'


def test_default_contructor(monkeypatch):
    monkeypatch.setattr(serial.Serial, "read_until", mock_read_until)
    fp = serial.Serial("/dev/pts/0")

    control = pyvpsu_control.VPSUControl(fp=fp)

    control.enable( pyvpsu_control.A8_Thrusters )
    control.enable( pyvpsu_control.A9_Thrusters )
    control.enable( pyvpsu_control.All_Thrusters )

    control.enable( pyvpsu_control.A8_Arms )
    control.enable( pyvpsu_control.All )

    control.disable( pyvpsu_control.A8_Thrusters )
    control.disable( pyvpsu_control.A9_Thrusters )
    control.disable( pyvpsu_control.All_Thrusters )

    control.disable( pyvpsu_control.A8_Arms )
    control.disable( pyvpsu_control.All )

    status = control.status()
    assert isinstance( status, dict )
